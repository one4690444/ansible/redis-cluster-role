Redis role
=========

Role Variables
--------------
Переменные со значениями по умолчанию:

    redis_enablerepo: remi
  Используется только для RHEL/Centos

    redis_port: 6379
    redis_bind_interface: 127.0.0.1
  Порт и интерфейс

    redis_unixsocket: ''
  Локальный Unix socket

    redis_timeout: 300
  Таймаут

    redis_loglevel: "notice"
    redis_logfile: /var/log/redis/redis-server.log
Log level and log location (valid levels are `debug`, `verbose`, `notice`, and `warning`).

Память:

    redis_maxmemory: 0
0 - неограниченная память

    redis_maxmemory_policy: "noeviction"
    redis_maxmemory_samples: 5

