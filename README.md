Install
-------

Install dependency, example

```
ansible-galaxy install -r requirements.yml
```

Run playbook

```
ansible-playbook -i inventory/infra.ini playbooks/setup/redis.yml -bK -u root --ask-pass
```